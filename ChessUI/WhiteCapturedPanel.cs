﻿using ChessClassLibrary;
using ChessClassLibrary.Enums;
using System.Drawing;
using System.Windows.Forms;
using ChessUI.EventArgs;
using ChessUI.Helpers;

namespace ChessUI
{
    public partial class WhiteCapturedPanel : FlowLayoutPanel
    {
        private readonly Point _placementOffset = new Point(875, 50);

        public WhiteCapturedPanel()
        {
            InitializeComponent();

            Location = _placementOffset;
            FlowDirection = FlowDirection.LeftToRight;
            BorderStyle = BorderStyle.FixedSingle;
            Size = new Size(300, 200);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void OnPieceCaptured(object sender, PieceCapturedEventArgs e)
        {
            var piece = new PiecePictureBox
            {
                Size = new Size(50, 50),
                SizeMode = PictureBoxSizeMode.Zoom,
                Piece = e.CapturedPiece,
                Coordinates = e.CapturedPiece.Coordinates,
                Image = e.PieceCapturedPieceImage,
            };

            if (e.CapturedPiece.PieceColor == PieceColor.White)
            {
                Controls.Add(piece);
            }
        }
    }
}
