﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ChessClassLibrary.Enums;
using ChessUI.Properties;

namespace ChessUI.Helpers
{
    public static class ImageLoader
    {
        private static Dictionary<Tuple<PieceColor, PieceType>, Image> _images;

        public static Dictionary<Tuple<PieceColor, PieceType>, Image> PieceImages => _images ?? (_images = LoadImages());

        private static Dictionary<Tuple<PieceColor, PieceType>, Image> LoadImages()
        {
            return new Dictionary<Tuple<PieceColor, PieceType>, Image>
            {
                { Tuple.Create(PieceColor.White,PieceType.Pawn), (Image)Resources.ResourceManager.GetObject("white_pawn") },
                { Tuple.Create(PieceColor.White,PieceType.Rook), (Image)Resources.ResourceManager.GetObject("white_rook") },
                { Tuple.Create(PieceColor.White,PieceType.Knight), (Image)Resources.ResourceManager.GetObject("white_knight") },
                { Tuple.Create(PieceColor.White,PieceType.Bishop), (Image)Resources.ResourceManager.GetObject("white_bishop") },
                { Tuple.Create(PieceColor.White,PieceType.Queen), (Image)Resources.ResourceManager.GetObject("white_queen") },
                { Tuple.Create(PieceColor.White,PieceType.King), (Image)Resources.ResourceManager.GetObject("white_king") },

                { Tuple.Create(PieceColor.Black,PieceType.Pawn), (Image)Resources.ResourceManager.GetObject("black_pawn") },
                { Tuple.Create(PieceColor.Black,PieceType.Rook), (Image)Resources.ResourceManager.GetObject("black_rook") },
                { Tuple.Create(PieceColor.Black,PieceType.Knight), (Image)Resources.ResourceManager.GetObject("black_knight") },
                { Tuple.Create(PieceColor.Black,PieceType.Bishop), (Image)Resources.ResourceManager.GetObject("black_bishop") },
                { Tuple.Create(PieceColor.Black,PieceType.Queen), (Image)Resources.ResourceManager.GetObject("black_queen") },
                { Tuple.Create(PieceColor.Black,PieceType.King), (Image)Resources.ResourceManager.GetObject("black_king") }
            };
        }

        public static Image GetMarkedImage()
        {
            return (Image) Resources.ResourceManager.GetObject("marked");
        }
    }
}
