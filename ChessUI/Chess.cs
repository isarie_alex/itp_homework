﻿using System.Windows.Forms;

namespace ChessUI
{
    public partial class Chess : Form
    {
        public BoardPanel BoardPanel { get; set; }
        public MovesListBox MovesListBox { get; set; }        
        public BlackCapturedPanel BlackCapturedPanel { get; set; }
        public WhiteCapturedPanel WhiteCapturedPanel { get; set; }

        public Chess()
        {
            BlackCapturedPanel = new BlackCapturedPanel();
            MovesListBox = new MovesListBox();
            WhiteCapturedPanel = new WhiteCapturedPanel();
            BoardPanel = new BoardPanel();            
            
            BoardPanel.PieceCaptured += MovesListBox.OnPieceCaptured;
            BoardPanel.PieceCaptured += BlackCapturedPanel.OnPieceCaptured;
            BoardPanel.PieceCaptured += WhiteCapturedPanel.OnPieceCaptured;
            BoardPanel.PieceMoved += MovesListBox.OnPieceMoved;

            Controls.Add(BoardPanel);
            Controls.Add(MovesListBox);
            Controls.Add(BlackCapturedPanel);
            Controls.Add(WhiteCapturedPanel);
            
            InitializeComponent();
        }
    }
}
