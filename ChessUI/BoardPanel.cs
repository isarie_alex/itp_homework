﻿using ChessClassLibrary;
using ChessUI.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ChessUI.EventArgs;

namespace ChessUI
{
    public partial class BoardPanel : Panel
    {
        public event EventHandler<MoveEventArgs> PieceMoved;        
        public event EventHandler<PieceCapturedEventArgs> PieceCaptured;

        public IEnumerable<Coordinates> AvailableMoves { get; set; }
        public BoardData BoardData { get; set; }
        public PiecePictureBox MoveSource { get; set; }
        public PiecePictureBox MoveDestination { get; set; }

        private readonly Point _placementOffset = new Point(50, 50);
        private readonly Size _cellSize = new Size(100, 100);

        private readonly Color _brightSpaceColor = Color.SandyBrown;
        private readonly Color _darkSpaceColor = Color.Brown;
        private readonly Color _markedColor = Color.BurlyWood;

        private readonly Image _markedImage = ImageLoader.GetMarkedImage();

        private const int MaxLines = 8;
        private const int MaxColumns = 8;


        public BoardPanel()
        {
            InitializeComponent();

            Size = new Size(_cellSize.Width * MaxLines, _cellSize.Height * MaxColumns);
            Location = _placementOffset;

            BoardData = new BoardData(new PieceFactory());
            BoardData.Initialize();

            CreateEmptyPiecePictureBoxes();
            UpdateViewFromBoardData();
        }

        private void UpdateViewFromBoardData()
        {
            for (var line = 0; line < MaxLines; line++)
            {
                for (var column = 0; column < MaxColumns; column++)
                {
                    var piecePictureBox = GetPiecePictureBoxAtPosition(new Coordinates(line, column));
                    piecePictureBox.Image = null;
                }
            }

            foreach (var position in BoardData.Pieces.Keys)
            {
                var pieceData = BoardData.Pieces[position];
                var piecePosition = BoardData.Pieces[position].Coordinates;
                var piecePictureBox = GetPiecePictureBoxAtPosition(piecePosition);
                piecePictureBox.Piece = pieceData;
                piecePictureBox.Image =
                    ImageLoader.PieceImages[
                        Tuple.Create(piecePictureBox.Piece.PieceColor, piecePictureBox.Piece.PieceType)];
            }
        }

        private PiecePictureBox GetPiecePictureBoxAtPosition(Coordinates coordinates)
        {
            PiecePictureBox result = null;
            foreach (Control control in Controls)
            {
                if (control is PiecePictureBox cell && cell.Coordinates.Equals(coordinates))
                {
                    result = cell;
                    break;
                }
            }

            return result;
        }

        private void CreateEmptyPiecePictureBoxes()
        {
            for (var line = 0; line < BoardData.Size; line++)
            {
                for (var column = 0; column < BoardData.Size; column++)
                {
                    var boardCell = new PiecePictureBox()
                    {
                        SizeMode = PictureBoxSizeMode.Zoom,
                        Size = _cellSize,
                        Location = new Point(column * _cellSize.Width, line * _cellSize.Height),
                        Coordinates = new Coordinates(line, column),
                        BackColor = (line + column) % 2 == 0 ? _brightSpaceColor : _darkSpaceColor
                    };

                    boardCell.Click += TryMove;

                    boardCell.Controls.Add(new Label() { Text = line + @"," + column, ForeColor = Color.Black, BackColor = Color.Transparent });
                    Controls.Add(boardCell);
                }
            }
        }

        private void TryMove(object sender, System.EventArgs e)
        {
            var clickedPictureBox = sender as PiecePictureBox;
            if (MoveSource == null && clickedPictureBox != null)
            {
                if (clickedPictureBox.Piece == null)
                {
                    return;
                }

                MoveSource = clickedPictureBox;
                AvailableMoves = clickedPictureBox.Piece.GetValidMovePositions(BoardData);
                MarkAvailableMoves();
            }
            else
            {
                if (clickedPictureBox != null && IsAvailableMove(clickedPictureBox.Coordinates))
                {
                    MoveDestination = clickedPictureBox;

                    PieceMoved?.Invoke(this, new MoveEventArgs(MoveSource.Piece, MoveDestination.Coordinates));

                    if (MoveDestination.Piece != null)
                    {
                        PieceCaptured?.Invoke(this,
                            new PieceCapturedEventArgs(MoveDestination.Piece,
                                ImageLoader.PieceImages[
                                    Tuple.Create(MoveDestination.Piece.PieceColor, MoveDestination.Piece.PieceType)
                                ]));
                    }
                    BoardData.Move(MoveSource.Piece, MoveDestination.Coordinates);

                    MoveSource.Piece = null;

                    UpdateViewFromBoardData();
                    RemoveMarkOnAvailableMoves();
                    MoveSource = null;
                    MoveDestination = null;
                }
                else
                {
                    RemoveMarkOnAvailableMoves();
                    MoveSource = null;
                }
            }
        }

        private bool IsAvailableMove(Coordinates coordinates)
        {
            return AvailableMoves.ToList().Contains(coordinates);
        }

        private void MarkAvailableMoves()
        {
            foreach (var position in AvailableMoves)
            {
                var cellToMark = GetPiecePictureBoxAtPosition(position);
                cellToMark.BackColor = _markedColor;
                cellToMark.BackgroundImage = _markedImage;
            }
        }

        private void RemoveMarkOnAvailableMoves()
        {
            foreach (var position in AvailableMoves)
            {
                var cellToMark = GetPiecePictureBoxAtPosition(position);
                cellToMark.BackColor = (position.Line + position.Column) % 2 == 0
                    ? _brightSpaceColor
                    : _darkSpaceColor;
                cellToMark.BackgroundImage = null;
            }
        }
    }
}
