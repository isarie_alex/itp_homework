﻿using System.Windows.Forms;
using ChessClassLibrary;
using ChessClassLibrary.Interfaces;

namespace ChessUI
{
    public partial class PiecePictureBox : PictureBox
    {
        public IPiece Piece { get; set; }
        public Coordinates Coordinates { get; set; }        

        public PiecePictureBox()
        {                        
            InitializeComponent();                        
        }
    }
}
