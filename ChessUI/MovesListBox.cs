﻿using ChessClassLibrary;
using System.Drawing;
using System.Windows.Forms;
using ChessUI.EventArgs;
using ChessUI.Helpers;

namespace ChessUI
{
    public partial class MovesListBox : ListBox
    {
        private readonly Point _placementOffset = new Point(875, 253);

        public MovesListBox()
        {
            InitializeComponent();
            Size = new Size(300, 400);
            Location = _placementOffset;            
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void OnPieceMoved(object sender, MoveEventArgs e)
        {
            Items.Add(e.PieceToMove + " (" + e.Source + " -> " + e.Destination + ")");
            SelectedIndex = Items.Count - 1;
        }

        public void OnPieceCaptured(object sender, PieceCapturedEventArgs e)
        {
            Items.Add(e.CapturedPiece + " was captured.");
            SelectedIndex = Items.Count - 1;
        }
    }
}
