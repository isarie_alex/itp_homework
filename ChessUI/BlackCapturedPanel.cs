﻿using System.Drawing;
using System.Windows.Forms;
using ChessClassLibrary;
using ChessClassLibrary.Enums;
using ChessUI.EventArgs;
using ChessUI.Helpers;

namespace ChessUI
{
    public partial class BlackCapturedPanel : FlowLayoutPanel
    {
           private readonly Point _placementOffset = new Point(875, 650);

        public BlackCapturedPanel()
        {
            Location = _placementOffset;
            FlowDirection = FlowDirection.LeftToRight;
            BorderStyle = BorderStyle.FixedSingle;
            Size = new Size(300, 200);
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void OnPieceCaptured(object sender, PieceCapturedEventArgs e)
        {
            var piece = new PiecePictureBox
            {
                Size = new Size(50, 50),
                SizeMode = PictureBoxSizeMode.Zoom,
                Piece = e.CapturedPiece,
                Coordinates = e.CapturedPiece.Coordinates,
                Image = e.PieceCapturedPieceImage,
            };

            if (e.CapturedPiece.PieceColor == PieceColor.Black)
            {
                Controls.Add(piece);
            }
        }
    }
}
