﻿using ChessClassLibrary;
using ChessClassLibrary.Interfaces;

namespace ChessUI.EventArgs
{
    public class MoveEventArgs : System.EventArgs
    {
        public Coordinates Source { get; }
        public Coordinates Destination { get; }
        public IPiece PieceToMove { get; }

        public MoveEventArgs(IPiece pieceToMove, Coordinates destination)
        {
            PieceToMove = pieceToMove;
            Source = pieceToMove.Coordinates;
            Destination = destination;
        }
    }
}