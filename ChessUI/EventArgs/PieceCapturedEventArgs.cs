﻿using ChessClassLibrary.Interfaces;
using System.Drawing;

namespace ChessUI.EventArgs
{
    public class PieceCapturedEventArgs : System.EventArgs
    {
        public IPiece CapturedPiece { get; }
        public Image PieceCapturedPieceImage { get; set; }

        public PieceCapturedEventArgs(IPiece capturedPiece, Image capturedPieceImage)
        {
            CapturedPiece = capturedPiece;
            PieceCapturedPieceImage = capturedPieceImage;
        }
    }
}