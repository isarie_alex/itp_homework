﻿using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;
using ChessClassLibrary.Pieces;

namespace ChessClassLibrary
{
    public class PieceFactory : IPieceFactory
    {
        public IPiece CreateBishop(PieceColor pieceColor, Coordinates coordinates)
        {
            return new Bishop(pieceColor, coordinates);
        }

        public IPiece CreateKing(PieceColor pieceColor, Coordinates coordinates)
        {
            return new King(pieceColor, coordinates);
        }

        public IPiece CreateKnight(PieceColor pieceColor, Coordinates coordinates)
        {
            return new Knight(pieceColor, coordinates);
        }

        public IPiece CreatePawn(PieceColor pieceColor, Coordinates coordinates)
        {
            return new Pawn(pieceColor, coordinates);
        }

        public IPiece CreateQueen(PieceColor pieceColor, Coordinates coordinates)
        {
            return new Queen(pieceColor, coordinates);
        }

        public IPiece CreateRook(PieceColor pieceColor, Coordinates coordinates)
        {
            return new Rook(pieceColor, coordinates);
        }
    }
}