﻿namespace ChessClassLibrary.Enums
{
    public enum PieceColor
    {        
        White = 1,
        Black = 2
    }
}