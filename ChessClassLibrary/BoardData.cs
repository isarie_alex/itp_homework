﻿using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;
using System.Collections.Generic;

namespace ChessClassLibrary
{
    public class BoardData : IBoardData
    {
        public IDictionary<Coordinates, IPiece> Pieces { get; }
        public int Size => 8;

        private readonly IPieceFactory _pieceFactory;

        public BoardData(IPieceFactory pieceFactory)
        {
            Pieces = new Dictionary<Coordinates, IPiece>();
            _pieceFactory = pieceFactory;
        }

        public void Initialize()
        {
            Coordinates coordinates;

            for (var column = 0; column < Size; column++)
            {
                Pieces.Add(coordinates = new Coordinates(6, column), _pieceFactory.CreatePawn(PieceColor.White, coordinates));
                Pieces.Add(coordinates = new Coordinates(1, column), _pieceFactory.CreatePawn(PieceColor.Black, coordinates));
            }

            Pieces.Add(coordinates = new Coordinates(7, 0), _pieceFactory.CreateRook(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(7, 7), _pieceFactory.CreateRook(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 0), _pieceFactory.CreateRook(PieceColor.Black, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 7), _pieceFactory.CreateRook(PieceColor.Black, coordinates));

            Pieces.Add(coordinates = new Coordinates(7, 1), _pieceFactory.CreateKnight(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(7, 6), _pieceFactory.CreateKnight(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 1), _pieceFactory.CreateKnight(PieceColor.Black, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 6), _pieceFactory.CreateKnight(PieceColor.Black, coordinates));

            Pieces.Add(coordinates = new Coordinates(7, 2), _pieceFactory.CreateBishop(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(7, 5), _pieceFactory.CreateBishop(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 2), _pieceFactory.CreateBishop(PieceColor.Black, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 5), _pieceFactory.CreateBishop(PieceColor.Black, coordinates));

            Pieces.Add(coordinates = new Coordinates(7, 3), _pieceFactory.CreateQueen(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 3), _pieceFactory.CreateQueen(PieceColor.Black, coordinates));

            Pieces.Add(coordinates = new Coordinates(7, 4), _pieceFactory.CreateKing(PieceColor.White, coordinates));
            Pieces.Add(coordinates = new Coordinates(0, 4), _pieceFactory.CreateKing(PieceColor.Black, coordinates));
        }

        public void Move(IPiece sourcePiece, Coordinates destination)
        {
            Pieces.TryGetValue(destination, out var destinationPiece);

            if (destinationPiece != null)
            {
                Pieces.Remove(destinationPiece.Coordinates);                
            }

            Pieces.Remove(sourcePiece.Coordinates);

            sourcePiece.Coordinates = destination;
            Pieces.Add(destination, sourcePiece);
        }
    }
}