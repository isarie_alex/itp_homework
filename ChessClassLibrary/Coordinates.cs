﻿using System;

namespace ChessClassLibrary
{
    public class Coordinates: IEquatable<Coordinates>
    {
        public int Line { get;  }
        public int Column { get;  }

        public Coordinates(int line, int column)
        {
            Line = line;
            Column = column;
        }

        public bool Equals(Coordinates other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Line == other.Line && Column == other.Column;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Coordinates) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Line * 397) ^ Column;
            }
        }

        public override string ToString()
        {
            return Line + "," + Column;
        }
    }
}