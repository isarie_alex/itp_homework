﻿using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;
using System.Collections.Generic;

namespace ChessClassLibrary.Pieces
{
    internal class Rook : IPiece
    {
        public PieceType PieceType { get; set; }
        public PieceColor PieceColor { get; set; }
        public Coordinates Coordinates { get; set; }

        public Rook(PieceColor pieceColor, Coordinates coordinates)
        {
            PieceType = PieceType.Rook;
            PieceColor = pieceColor;
            Coordinates = coordinates;
        }

        public IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData)
        {
            var result = new List<Coordinates>();

            AddVerticalMoves(boardData, result);
            AddHorizontalMoves(boardData, result);

            return result;
        }

        private void AddHorizontalMoves(BoardData boardData, List<Coordinates> result)
        {
            for (var column = Coordinates.Column - 1; column >= 0; column--)
            {
                var position = new Coordinates(Coordinates.Line, column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }

            for (var column = Coordinates.Column + 1; column < 8; column++)
            {
                var position = new Coordinates(Coordinates.Line, column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }
        }

        private void AddVerticalMoves(IBoardData boardData, ICollection<Coordinates> result)
        {
            for (var line = Coordinates.Line - 1; line >= 0; line--)
            {
                var position = new Coordinates(line, Coordinates.Column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }

            for (var line = Coordinates.Line + 1; line < 8; line++)
            {
                var position = new Coordinates(line, Coordinates.Column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }
        }

        private bool TryAddValidPosition(IBoardData boardDataData, Coordinates coordinates, ICollection<Coordinates> result)
        {
            var isOccupied = boardDataData.Pieces.ContainsKey(coordinates);
            var canAttack = isOccupied && boardDataData.Pieces[coordinates].PieceColor != PieceColor;

            if (!isOccupied || canAttack)
            {
                result.Add(coordinates);

                if (canAttack)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return PieceColor + " " + PieceType;
        }
    }
}