﻿using System.Collections.Generic;
using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;

namespace ChessClassLibrary.Pieces
{
    internal class Pawn : IPiece
    {
        public PieceType PieceType { get; set; }
        public PieceColor PieceColor { get; set; }
        public Coordinates Coordinates { get; set; }

        public Pawn(PieceColor pieceColor, Coordinates coordinates)
        {
            PieceType = PieceType.Pawn;
            PieceColor = pieceColor;
            Coordinates = coordinates;
        }

        public IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData)
        {
            var result = new List<Coordinates>();

            if (PieceColor == PieceColor.Black)
            {
                if (Coordinates.Line < 7)
                {
                    var position = new Coordinates(Coordinates.Line + 1, Coordinates.Column);
                    var isOccupied = boardData.Pieces.ContainsKey(position);
                    if (!isOccupied)
                    {
                        result.Add(position);
                    }
                }

                if (Coordinates.Line == 1)
                {
                    var position = new Coordinates(Coordinates.Line + 2, Coordinates.Column);
                    var isOccupied = boardData.Pieces.ContainsKey(position);
                    if (!isOccupied)
                    {
                        result.Add(position);
                    }
                }

                var frontRight = new Coordinates(Coordinates.Line+1, Coordinates.Column+1);
                var canAttackRight = boardData.Pieces.ContainsKey(frontRight) &&
                                     boardData.Pieces[frontRight].PieceColor != PieceColor;
                if (canAttackRight)
                {
                    result.Add(frontRight);
                }

                var frontLeft = new Coordinates(Coordinates.Line+1, Coordinates.Column-1);
                var canAttackLeft = boardData.Pieces.ContainsKey(frontLeft) &&
                                    boardData.Pieces[frontRight].PieceColor != PieceColor;
                if (canAttackLeft)
                {
                    result.Add(frontLeft);
                }
            }

            if (PieceColor == PieceColor.White)
            {
                if (Coordinates.Line > 0)
                {
                    var position = new Coordinates(Coordinates.Line - 1, Coordinates.Column);
                    var isOccupied = boardData.Pieces.ContainsKey(position);
                    if (!isOccupied)
                    {
                        result.Add(position);
                    }
                }

                if (Coordinates.Line == 6)
                {
                    var position = new Coordinates(Coordinates.Line - 2, Coordinates.Column);
                    var isOccupied = boardData.Pieces.ContainsKey(position);
                    if (!isOccupied)
                    {
                        result.Add(position);
                    }
                }

                var frontRight = new Coordinates(Coordinates.Line - 1, Coordinates.Column + 1);
                var canAttackRight = boardData.Pieces.ContainsKey(frontRight) &&
                                     boardData.Pieces[frontRight].PieceColor != PieceColor;
                if (canAttackRight)
                {
                    result.Add(frontRight);
                }

                var frontLeft = new Coordinates(Coordinates.Line - 1, Coordinates.Column - 1);
                var canAttackLeft = boardData.Pieces.ContainsKey(frontLeft) &&
                                    boardData.Pieces[frontRight].PieceColor != PieceColor;
                if (canAttackLeft)
                {
                    result.Add(frontLeft);
                }
            }

            return result;
        }

        public override string ToString()
        {
            return PieceColor + " " + PieceType;
        }
    }
}