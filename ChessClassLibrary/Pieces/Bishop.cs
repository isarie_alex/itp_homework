﻿using System.Collections.Generic;
using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;

namespace ChessClassLibrary.Pieces
{
    internal class Bishop : IPiece
    {
        public PieceType PieceType { get; set; }
        public PieceColor PieceColor { get; set; }
        public Coordinates Coordinates { get; set; }

        public Bishop(PieceColor pieceColor, Coordinates coordinates)
        {
            PieceType = PieceType.Bishop;
            PieceColor = pieceColor;
            Coordinates = coordinates;
        }

        public IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData)
        {
            var result = new List<Coordinates>();


            var line = Coordinates.Line;
            var column = Coordinates.Column;
            do
            {
                line++;
                column++;
                if (TryAddValidPosition(boardData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);

            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line--;
                column--;
                if (TryAddValidPosition(boardData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);


            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line--;
                column++;
                if (TryAddValidPosition(boardData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);

            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line++;
                column--;
                if (TryAddValidPosition(boardData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);

            return result;
        }

        private bool TryAddValidPosition(IBoardData boardDataData, Coordinates coordinates, ICollection<Coordinates> result)
        {
            var positionInBounds = coordinates.Line >= 0 && coordinates.Line < 8 && coordinates.Column >= 0 && coordinates.Column < 8;

            if (!positionInBounds) return true;

            var isOccupied = boardDataData.Pieces.ContainsKey(coordinates);
            var canAttack = isOccupied && boardDataData.Pieces[coordinates].PieceColor != PieceColor;

            if (!isOccupied || canAttack)
            {
                result.Add(coordinates);
                if (canAttack)
                {
                    return true;
                }
            }

            else
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return PieceColor + " " + PieceType;
        }
    }


}