﻿using System.Collections.Generic;
using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;

namespace ChessClassLibrary.Pieces
{
    internal class King : IPiece
    {
        public PieceType PieceType { get; set; }
        public PieceColor PieceColor { get; set; }
        public Coordinates Coordinates { get; set; }


        public King(PieceColor pieceColor, Coordinates coordinates)
        {
            PieceType = PieceType.King;
            PieceColor = pieceColor;
            Coordinates = coordinates;
        }

        public IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData)
        {
            var result = new List<Coordinates>();
            var positions = new List<Coordinates>()
            {
                new Coordinates(Coordinates.Line+1,Coordinates.Column+1),
                new Coordinates(Coordinates.Line+1,Coordinates.Column-1),
                new Coordinates(Coordinates.Line-1,Coordinates.Column+1),
                new Coordinates(Coordinates.Line-1,Coordinates.Column-1),
                new Coordinates(Coordinates.Line,Coordinates.Column+1),
                new Coordinates(Coordinates.Line,Coordinates.Column-1),
                new Coordinates(Coordinates.Line+1,Coordinates.Column),
                new Coordinates(Coordinates.Line-1,Coordinates.Column),
            };

            foreach (var position in positions)
            {
                var positionInBounds = position.Line >= 0 && position.Line < 8 && position.Column >= 0 && position.Column < 8;

                if (!positionInBounds) continue;

                var isOccupied = boardData.Pieces.ContainsKey(position);
                var canAttack = isOccupied && boardData.Pieces[position].PieceColor != PieceColor;

                if (!isOccupied || canAttack)
                {
                    result.Add(position);
                }
            }
            return result;
        }

        public override string ToString()
        {
            return PieceColor + " " + PieceType;
        }
    }
}