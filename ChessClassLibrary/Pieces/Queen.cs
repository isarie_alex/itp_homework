﻿using ChessClassLibrary.Enums;
using ChessClassLibrary.Interfaces;
using System.Collections.Generic;

namespace ChessClassLibrary.Pieces
{
    internal class Queen : IPiece
    {
        public PieceType PieceType { get; set; }
        public PieceColor PieceColor { get; set; }
        public Coordinates Coordinates { get; set; }

        public Queen(PieceColor pieceColor, Coordinates coordinates)
        {
            PieceType = PieceType.Queen;
            PieceColor = pieceColor;
            Coordinates = coordinates;
        }

        public IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData)
        {
            var result = new List<Coordinates>();

            AddVerticalMoves(boardData, result);
            AddHorizontalMoves(boardData, result);
            AddDiagonalMoves(boardData, result);

            return result;
        }

        private void AddHorizontalMoves(IBoardData boardData, ICollection<Coordinates> result)
        {
            for (var column = Coordinates.Column - 1; column >= 0; column--)
            {
                var position = new Coordinates(Coordinates.Line, column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }

            for (var column = Coordinates.Column + 1; column < 8; column++)
            {
                var position = new Coordinates(Coordinates.Line, column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }
        }

        private void AddVerticalMoves(IBoardData boardData, ICollection<Coordinates> result)
        {
            for (var line = Coordinates.Line - 1; line >= 0; line--)
            {
                var position = new Coordinates(line, Coordinates.Column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }

            for (var line = Coordinates.Line + 1; line < 8; line++)
            {
                var position = new Coordinates(line, Coordinates.Column);
                if (TryAddValidPosition(boardData, position, result)) break;
            }
        }

        private void AddDiagonalMoves(IBoardData boardDataData, ICollection<Coordinates> result)
        {
            var line = Coordinates.Line;
            var column = Coordinates.Column;
            do
            {
                line++;
                column++;
                if (TryAddValidPosition(boardDataData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);

            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line--;
                column--;
                if (TryAddValidPosition(boardDataData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);


            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line--;
                column++;
                if (TryAddValidPosition(boardDataData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);

            line = Coordinates.Line;
            column = Coordinates.Column;
            do
            {
                line++;
                column--;
                if (TryAddValidPosition(boardDataData, new Coordinates(line, column), result))
                {
                    break;
                }
            } while (true);
        }

        private bool TryAddValidPosition(IBoardData boardDataData, Coordinates coordinates, ICollection<Coordinates> result)
        {
            var positionInBounds = coordinates.Line >= 0 && coordinates.Line < 8 && coordinates.Column >= 0 && coordinates.Column < 8;

            if (!positionInBounds) return true;

            var isOccupied = boardDataData.Pieces.ContainsKey(coordinates);
            var canAttack = isOccupied && boardDataData.Pieces[coordinates].PieceColor != PieceColor;

            if (!isOccupied || canAttack)
            {
                result.Add(coordinates);

                if (canAttack)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return PieceColor + " " + PieceType;
        }
    }
}