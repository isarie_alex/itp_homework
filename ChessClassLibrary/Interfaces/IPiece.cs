﻿using System.Collections.Generic;
using ChessClassLibrary.Enums;

namespace ChessClassLibrary.Interfaces
{
    public interface IPiece
    {
        PieceColor PieceColor { get; set; }
        PieceType PieceType { get; set; }
        Coordinates Coordinates { get; set; }

        IEnumerable<Coordinates> GetValidMovePositions(BoardData boardData);        
    }
}