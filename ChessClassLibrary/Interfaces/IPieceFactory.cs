﻿using ChessClassLibrary.Enums;

namespace ChessClassLibrary.Interfaces
{
    public interface IPieceFactory
    {
        IPiece CreatePawn(PieceColor pieceColor, Coordinates coordinates);

        IPiece CreateRook(PieceColor pieceColor, Coordinates coordinates);

        IPiece CreateKnight(PieceColor pieceColor, Coordinates coordinates);

        IPiece CreateBishop(PieceColor pieceColor, Coordinates coordinates);

        IPiece CreateQueen(PieceColor pieceColor, Coordinates coordinates);

        IPiece CreateKing(PieceColor pieceColor, Coordinates coordinates);
    }
}