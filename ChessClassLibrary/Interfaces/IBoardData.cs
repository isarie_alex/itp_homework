﻿using System.Collections.Generic;

namespace ChessClassLibrary.Interfaces
{
    internal interface IBoardData
    {
        int Size { get; }
        IDictionary<Coordinates, IPiece> Pieces { get; }

        void Initialize();
    }
}